package types

type LineReports struct {
	Disruptions []Disruption `json:"disruptions"`
	LineReports []LineReport `json:"line_reports"`
}

type LineReport struct {
	Line      Line       `json:"line"`
	PTObjects []PTObject `json:"pt_objects"`
}
