package api

import (
	"flag"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

const IDFM_BASEURL = "https://prim.iledefrance-mobilites.fr/marketplace"

var IDFM_TOKEN = ""

func init() {
	flag.StringVar(&IDFM_TOKEN, "token-idfm", IDFM_TOKEN, "Token to access IDFM API")
}

type apiMetadata struct {
	Call    string    `json:"call"`
	Date    time.Time `json:"date"`
	Version int       `json:"version"`
}

type apiResult struct {
	Result   interface{} `json:"result"`
	Metadata apiMetadata `json:"_metadata"`
}

func APIResult(c *gin.Context, res interface{}) apiResult {
	return apiResult{
		Result: res,
		Metadata: apiMetadata{
			Call:    fmt.Sprintf("%s %s", c.Request.Method, c.Request.URL.Path),
			Date:    time.Now(),
			Version: 4,
		},
	}
}
