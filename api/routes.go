package api

import (
	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/idfm-api/config"
)

func DeclareRoutes(router *gin.Engine, cfg *config.Config) {
	apiRoutes := router.Group("/api")

	declareDestinationsRoutes(apiRoutes)
	declareLinesRoutes(apiRoutes)
	declareMissionsRoutes(apiRoutes)
	declareSchedulesRoutes(apiRoutes)
	declareStationsRoutes(apiRoutes)
	declareTrafficRoutes(apiRoutes)
}
