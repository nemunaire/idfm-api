package api

import (
	"time"
)

type IDFMTime time.Time

func (t *IDFMTime) UnmarshalJSON(b []byte) error {
	tmp, err := time.Parse("\"2006-01-02T15:04\"", string(b))
	*t = IDFMTime(tmp)
	return err
}

func (t IDFMTime) MarshalJSON() ([]byte, error) {
	return []byte(time.Time(t).Format("\"2006-01-02T15:04\"")), nil
}

func (t *IDFMTime) After(d IDFMTime) bool {
	return time.Time(*t).After(time.Time(d))
}

func (t IDFMTime) String() string {
	return time.Time(t).String()
}
