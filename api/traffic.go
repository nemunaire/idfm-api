package api

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strings"

	navitia "git.nemunai.re/nemunaire/idfm-api/types"
	"github.com/gin-gonic/gin"
)

type PGTraffic struct {
	Line    string `json:"line"`
	Slug    string `json:"slug"`
	Title   string `json:"title"`
	Message string `json:"message"`
}

func effectGradation(effect navitia.Effect) int {
	switch effect {
	case navitia.JourneyStatusUnknownEffect:
		return 9
	case navitia.EffectNoService:
		return 7
	case navitia.JourneyStatusReducedService:
		return 6
	case navitia.JourneyStatusSignificantDelay:
		return 5
	case navitia.JourneyStatusDetour:
		return 4
	case navitia.JourneyStatusAdditionalService:
		return 3
	case navitia.JourneyStatusOtherEffect:
		return 2
	case navitia.JourneyStatusStopMoved:
		return 1
	default:
		return 0
	}
}

func declareTrafficRoutes(router *gin.RouterGroup) {
	router.GET("/traffic/:type/:code", func(c *gin.Context) {
		code := searchLine(convertLineType(string(c.Param("type"))), convertLineCode(string(c.Param("code"))))

		rurl, err := url.JoinPath(IDFM_BASEURL, "v2/navitia/lines", "line:IDFM:"+code, "line_reports")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		requrl, err := url.Parse(rurl)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		req, err := http.NewRequest("GET", requrl.String(), nil)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		req.Header.Add("Accept", "application/json")
		req.Header.Add("apikey", IDFM_TOKEN)

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}
		defer res.Body.Close()

		if res.StatusCode != http.StatusOK {
			c.DataFromReader(res.StatusCode, res.ContentLength, res.Header.Get("content-type"), res.Body, nil)
			return
		}

		var traffic navitia.LineReports
		dec := json.NewDecoder(res.Body)
		if err = dec.Decode(&traffic); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		pgt := PGTraffic{
			Line:    code,
			Slug:    "normal", // normal_trav alerte critique
			Title:   "",
			Message: "",
		}

		slug := 0

		for _, disruption := range traffic.Disruptions {
			// Ignore past or future disruptions
			if disruption.Status != navitia.StatusActive {
				continue
			}

			// Ignore lift issues
			if strings.Contains(strings.Join(disruption.Tags, ","), "Ascenseur") {
				continue
			}

			for _, m := range disruption.Messages {
				if strings.Contains(strings.Join(m.Channel.Types, ","), "title") {
					if pgt.Title != "" {
						pgt.Title += ", "
					}
					pgt.Title += m.Text
				} else if strings.Contains(strings.Join(m.Channel.Types, ","), "web") {
					if pgt.Message != "" {
						pgt.Message += "<hr>"
					}
					pgt.Message += m.Text
				}
			}

			if disruption.Cause == "travaux" {
				pgt.Slug = "normal_trav"
			} else {
				icr := effectGradation(disruption.Severity.Effect)
				if icr > slug {
					slug = icr
					switch disruption.Severity.Effect {
					case navitia.JourneyStatusUnknownEffect:
						pgt.Slug = "critique"
					case navitia.EffectNoService:
						pgt.Slug = "critique"
					case navitia.JourneyStatusReducedService:
						pgt.Slug = "alerte"
					case navitia.JourneyStatusSignificantDelay:
						pgt.Slug = "alerte"
					case navitia.JourneyStatusDetour:
						pgt.Slug = "alerte"
					case navitia.JourneyStatusAdditionalService:
						pgt.Slug = "alerte"
					case navitia.JourneyStatusOtherEffect:
						pgt.Slug = "alerte"
					case navitia.JourneyStatusStopMoved:
						pgt.Slug = "alerte"
					default:
						pgt.Slug = "normal"
					}
				}
			}

		}

		if pgt.Title == "" {
			pgt.Title = "Trafic normal"
		}
		if pgt.Message == "" {
			pgt.Message = "Trafic normal sur l'ensemble de la ligne."
		}

		c.JSON(http.StatusOK, APIResult(c, pgt))
	})
}
