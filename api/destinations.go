package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type IDFMSchedule struct {
	Line struct {
		Id        string `json:"id"`
		Label     string `json:"label"`
		Name      string `json:"name"`
		ShortName string `json:"shortName"`
		Companies []struct {
			Id    string `json:"id"`
			Label string `json:"label"`
		} `json:"companies"`
		Network struct {
			Id    string `json:"id"`
			Label string `json:"label"`
		} `json:"network"`
		Mode      string `json:"mode"`
		ModeLabel string `json:"modeLabel"`
		Realtime  bool   `json:"realtime"`
		UFR       bool   `json:"ufr"`
		Visual    bool   `json:"visual"`
		Sound     bool   `json:"sound"`
		Color     string `json:"color"`
		TextColor string `json:"textColor"`
	} `json:"line"`
	Schedules []struct {
		RouteId string `json:"routeId"`
		From    string `json:"from"`
		To      string `json:"to"`
		First   string `json:"first"`
		Last    string `json:"last"`
	} `json:"schedules"`
	Plans []struct {
		Link  string `json:"link"`
		Label string `json:"label"`
	} `json:"plans"`
	ScheduleDocs []struct {
		Link  string `json:"link"`
		Label string `json:"label"`
	} `json:"scheduleDocs"`
	CurrentIT []struct {
		Id              string   `json:"id"`
		Title           string   `json:"title"`
		Message         string   `json:"message"`
		ImpactStartTime IDFMTime `json:"impactStartTime"`
		ImpactEndTime   IDFMTime `json:"impactEndTime"`
		Severity        int      `json:"severity"`
		Type            int      `json:"type"`
	} `json:"currentIT"`
}

type PGDestination struct {
	Name string `json:"name"`
	Way  string `json:"way"`
}

func getDestinations(code string) ([]PGDestination, error) {
	return nil, nil
}

func declareDestinationsRoutes(router *gin.RouterGroup) {
	router.GET("/destinations/:type/:code", func(c *gin.Context) {
		pgd, err := getDestinations(string(c.Param("code")))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, APIResult(c, map[string][]PGDestination{
			"destinations": pgd,
		}))
	})
}
