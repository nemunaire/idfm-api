package main

import (
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"git.nemunai.re/nemunaire/idfm-api/config"
)

var (
	Version = "custom-build"
)

func main() {
	seed := time.Now().Unix()
	rand.Seed(seed)

	cfg, err := config.Consolidated()
	if err != nil {
		log.Fatal("Unable to read configuration:", err)
	}

	// Start app
	a := NewApp(cfg)
	go a.Start()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	log.Println("Stopping the service...")
	a.Stop()
	log.Println("Stopped")
}
