package config

import (
	"encoding/base64"
	"net/url"
)

type JWTSecretKey []byte

func (i *JWTSecretKey) String() string {
	return base64.StdEncoding.EncodeToString(*i)
}

func (i *JWTSecretKey) Set(value string) error {
	z, err := base64.StdEncoding.DecodeString(value)
	if err != nil {
		return err
	}

	*i = z
	return nil
}

type URL struct {
	URL *url.URL
}

func (i *URL) String() string {
	if i.URL != nil {
		return i.URL.String()
	} else {
		return ""
	}
}

func (i *URL) Set(value string) error {
	u, err := url.Parse(value)
	if err != nil {
		return err
	}

	i.URL = u
	return nil
}
