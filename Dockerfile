FROM golang:1-alpine AS build

COPY . /go/src/git.nemunai.re/idfm-api
WORKDIR /go/src/git.nemunai.re/idfm-api
RUN go get -v && go generate -v && go build -v -ldflags="-s -w"


FROM alpine:3.21

EXPOSE 8080
CMD ["/srv/idfm-api"]

RUN apk add --no-cache tzdata

COPY --from=build /go/src/git.nemunai.re/idfm-api/idfm-api /srv/idfm-api
